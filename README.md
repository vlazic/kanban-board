
# **Kanban application in React/Redux + Typescript stack**

## Features

- Ability to add new to-do items.
- Sort and change the status of individual items with 'drag and drop' functionality.
- Upon entering a to-do, the item is displayed on the page with a creation date/time.
- It is possible to edit and delete to-do items
- A to-do item consists of 4 separate pieces of user-defined information: a title, a detailed description, the person assigned to complete the to-do, and completion status (valid statuses 'To-do', 'In Progress', 'Done').
- If the item is edited it will update a count of the number of times the item has been changed and a 'last edited' date/time.
- It is possible to sort the to-do items by each type of information.

## Notes and links

To-do items do not persist across multiple sessions.

Each time page is refreshed, the app will auto-generate new tasks.

Demo: [https://kanban.fs.rs/](https://kanban.fs.rs/)

## Technologies

This project is bootstrapped with **Create React App** (CRA) and **TypeScript** CRA template.

For project styling, I've pickedthe **Bootstrap v4** framework and the **Styled Components** library.

For application **state** management I've decided to use the **Redux** library. Redux store is configured in _store.ts_ file and other Redux related files are located in _actions/_, _reducers/_ and _selectors/_ folders.

For drag-and-drop functionality, I've integrated **React Beautiful DND** library and for toast messages **React-Toastify** library.