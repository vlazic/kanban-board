import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ToastContainer, toast } from "react-toastify";

import App from "./components/App";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.css";
import "./index.css";
import store from "./store";

ReactDOM.render(
  <Provider store={store}>
    <App />
    <ToastContainer position={toast.POSITION.BOTTOM_RIGHT} />
  </Provider>,
  document.getElementById("root")
);
