export type IDatetime = number;
export type ITodoStatus = "TODO" | "IN_PROGRES" | "DONE";

export interface ITodo {
  title: string;
  description: string;
  status: ITodoStatus;
  assigned_to: string;
  created_at: IDatetime;
  updated_at: IDatetime;
  number_of_edits: number;
}

export type IUpdatingTodo = Omit<ITodo, "updated_at" | "number_of_edits">;
