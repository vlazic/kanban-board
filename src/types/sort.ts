import { ITodo } from "./todo";

export type ISortKey = keyof ITodo | "custom";

export type ISortOrder = "ASC" | "DESC";
