import { IState } from "../reducers";

export const getTodos = (state: IState) => state.todos;
