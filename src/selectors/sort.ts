import { IState } from "../reducers";

export const getSortState = (state: IState) => state.sort;
export const getSortOrder = (state: IState) => state.sort.order;
export const getSortKey = (state: IState) => state.sort.key;
